import 'dart:convert';

import 'package:ddholidaysinterview/helpers/constants.dart';
import 'package:ddholidaysinterview/models/experience.dart';
import 'package:ddholidaysinterview/widgets/plan_card.dart';
import 'package:ddholidaysinterview/widgets/plan_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class TripPlan extends StatefulWidget {
  @override
  _TripPlanState createState() => _TripPlanState();
}

class _TripPlanState extends State<TripPlan> {
  List<Experience> _experiences = List();
  bool _showCards = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getTripPlans();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 15.0,
        ),
        Container(
          child: Text(
            'Hi Hossein',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25.0,
            ),
          ),
          padding: EdgeInsets.symmetric(
            horizontal: 10,
          ),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                child: Text(
                  'Your Dubai trip plan',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                ),
              ),
            ),
            IconButton(
              icon: _showCards
                  ? Icon(Icons.view_week)
                  : Icon(Icons.center_focus_strong),
              onPressed: toggleIcons,
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return _showCards
                  ? PlanCard(
                      name: _experiences[index].name,
                      description: _experiences[index].description,
                      date: _experiences[index].date,
                      passengers: _experiences[index].passengers[0],
                      time: _experiences[index].time,
                      imageUrl: _experiences[index].images[0],
                    )
                  : PlanDetails(
                      date: _experiences[index].date,
                      name: _experiences[index].name,
                      time: _experiences[index].time,
                    );
            },
            itemCount: _experiences.length,
            scrollDirection: Axis.horizontal,
          ),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height / 4,
        ),
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }

  /// it sends request to get tripPlans & parses response to create cards list
  getTripPlans() async {
    final response = await http.get(TRIP_PLANS_URI);
    dynamic data = jsonDecode(response.body)['data'] ?? '';
    for (dynamic subData in data) {
      String date = subData['date'] ?? '';
      for (dynamic ex in subData['experiences']) {
        ex['date'] = date;
        _experiences.add(Experience.fromJson(ex as Map<String, dynamic>));
      }
    }
    setState(() {});
  }

  toggleIcons() {
    if (_showCards) {
      setState(() {
        _showCards = false;
      });
    } else {
      setState(() {
        _showCards = true;
      });
    }
  }
}
