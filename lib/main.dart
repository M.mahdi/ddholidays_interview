import 'package:ddholidaysinterview/trip_plan.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: Colors.white,
      ),
      home: MyHomePage(title: '11:38 AM - Dubai'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  TabController _controller;
  int _selectedPage = 0;
  List<Widget> _pages = [
    Container(
      child: TripPlan(),
      key: Key('mainContainer'),
    ),
    Container(
      child: Center(
        child: Text('Page 2'),
      ),
    ),
    Container(
      child: Center(
        child: Text('Page 3'),
      ),
    ),
  ];

  @override
  void initState() {
    super.initState();
    _controller =
        TabController(length: 3, vsync: this, initialIndex: _selectedPage);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(
            fontSize: 14.0,
          ),
        ),
        actions: [
          Row(
            children: [
              Container(
                child: Stack(
                  children: [
                    Icon(
                      Icons.notifications,
                      size: 25,
                    ),
                    Positioned(
                      child: Container(
                        child: Text(
                          '99+',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12.0,
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          color: Colors.red,
                        ),
                      ),
                      top: 0.0,
                      left: 0.0,
                    ),
                  ],
                ),
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
          ),
        ],
        elevation: 0.0,
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Container(),
      ),
      body: _pages[_selectedPage],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.calendar_today),
            title: Text('1'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.center_focus_weak),
            title: Text('2'),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.calendar_today),
            title: Text('3'),
          ),
        ],
        onTap: (int index) {
          setState(() {
            _selectedPage = index;
          });
        },
        currentIndex: _selectedPage,
        showUnselectedLabels: false,
        showSelectedLabels: false,
        unselectedItemColor: Colors.purple,
        selectedItemColor: Colors.black12,
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
