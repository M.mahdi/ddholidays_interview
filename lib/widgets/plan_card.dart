import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlanCard extends StatelessWidget {
  final String name;
  final String description;
  final String date;
  final String passengers;
  final String time;
  final String imageUrl;

  PlanCard(
      {@required this.name,
      @required this.description,
      @required this.date,
      @required this.passengers,
      @required this.time,
      @required this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        child: Container(
          child: Row(
            children: <Widget>[
              Container(
                child: Image.network(
                  imageUrl,
                  fit: BoxFit.cover,
                ),
                height: 100,
                width: 80,
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Text(
                      name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.body2,
                    ),
                    Text(
                      description,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.caption,
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.calendar_today,
                          size: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(date),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.person,
                          size: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(passengers),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.watch_later,
                          size: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(time),
                      ],
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              ),
            ],
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          padding: EdgeInsets.all(5.0),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        elevation: 5,
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 20.0,
      ),
      width: MediaQuery.of(context).size.width,
    );
  }
}
