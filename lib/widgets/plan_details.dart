import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlanDetails extends StatelessWidget {
  final String name;
  final String date;
  final String time;

  PlanDetails({@required this.name, @required this.date, @required this.time});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Text('...........'),
          SizedBox(
            width: 10,
          ),
          Column(
            children: <Widget>[
              Text(
                name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.body2,
              ),
              SizedBox(
                height: 5.0,
              ),
              Chip(
                label: Text(
                  time,
                ),
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                date,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
          SizedBox(
            width: 10,
          ),
          Text('...........'),
        ],
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 20.0,
      ),
      width: MediaQuery.of(context).size.width,
    );
  }
}
