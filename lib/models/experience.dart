class Experience {
  String date;
  String time;
  String type;
  String name;
  String price;
  String description;

  List<dynamic> images;
  List<dynamic> passengers;

  Experience.fromJson(Map<String, dynamic> json)
      : time = json['time'] ?? '',
        date = json['date'] ?? '',
        type = json['data']['type'] ?? '',
        name = json['data']['name'] ?? '',
        price = json['data']['price'] ?? '',
        images = json['data']['images'] ?? '',
        passengers = json['data']['passengers'] ?? '',
        description = json['data']['description'] ?? '';
}
